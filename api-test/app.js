var express = require('express');
var app = express();
var request = require("request");
var bodyParser = require('body-parser');
var username = 'pai.a@husky.neu.edu';
var password = 'abcd1234';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//solve CORS problem
/*app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    // Request headers you wish to allow
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Pass to next layer of middleware
    next();
});*/

//get info of a specific repo
//get info of all repos
app.get('/mm/bitbucket/:repoinfo', function(req, res){
	console.log(req.query.ownerame);
	var urlstr = "https://api.bitbucket.org/2.0/repositories/"
	if (req.query.reponame === undefined)
		urlstr += req.query.ownername;
	else
		urlstr += req.query.ownername +'/'+req.query.reponame;
	request({
		uri: urlstr,
		auth: {
    		user: username,
    		password: password
  		}
	}, function(err, response, body) {
		if (err) {
			console.dir(err);
			return;
		}
		console.dir('headers', res.headers);
		console.dir('status code', res.statusCode);
		console.dir(body);
		res.send(body);

	});
})
//add a member to a group
//test: http://localhost:3000/mm/bitbucket/addMember?aname=anpspkteam&mname=irisyu
app.put('/mm/bitbucket/:addMember', function(req,res){
	var urlstr = "https://api.bitbucket.org/1.0/groups/"+req.query.aname+"/developers/members/"+req.query.mname;
	request({
		url: urlstr,
		method: 'PUT',
		auth:{
			user:username,
			password:password
		},
		body:{},
		json: true
	},function(err,response, body){
		if(err) {
			console.dir(err);
			return;
		}
		console.dir('status code', res.statusCode);
		console.dir(body);
		res.send(body);
	})
	
});


///remove a member from a group
//test: http://localhost:3000/mm/bitbucket/removeMember?aname=anpspkteam&mname=irisyu
app.delete('/mm/bitbucket/:removeMember', function(req,res){
    var urlstr = "https://api.bitbucket.org/1.0/groups/"+req.query.aname+"/developers/members/"+req.query.mname;
    request({
        url: urlstr,
        method: 'DELETE',
        auth:{
            user:username,
            password:password
        },
        body:{},
        json: true
    },function(err,response, body){
        if(err) {
            console.dir(err);
            return;
        }
        console.dir('status code', res.statusCode);
        console.dir(body);
        res.send(body);
    })
});



//create a new repo
//http://localhost:3000/mm/bitbucket/createRepo?aname=anpspk&rslug=test2
app.post('/mm/bitbucket/:createRepo', function(req, res){
	var urlstr = "https://api.bitbucket.org/2.0/repositories/"+req.query.aname+"/"+req.query.rslug;
	request({
		url: urlstr,
		method: 'POST',
		auth:{
			user:username,
			password:password
		},
		json: {
        	"scm": "git", 
        	"is_private": "true", 
        	"fork_policy": "no_public_forks"
    	}
	},function(err,response, body){
		if(err) {
			console.dir(err);
			return;
		}
		console.dir('status code', res.statusCode);
		console.dir(body);
		res.send(body);
	})

});


app.listen(3000, function () {
  console.log('API test app listening on port 3000.');
});